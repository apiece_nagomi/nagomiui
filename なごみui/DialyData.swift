//
//  DialyData.swift
//  なごみui
//
//  Created by yu yamashiro on 2015/12/15.
//  Copyright © 2015年 yuyamashiro. All rights reserved.
//

import Foundation
import UIKit

class DialyData: NSObject {
    var Title: String?
    var Text: String?
    var Image: String?
    
    var dateString:String?
    
    
    init(date:NSDate,title:String,text:String,imageString:String) {
        self.Title = title
        self.Text = text
        self.Image = imageString
        
        let dateformat = NSDateFormatter()
        dateformat.dateFormat = "MM/dd"
        self.dateString = dateformat.stringFromDate(date)
    }
    
}

//ダミーデータ
var dialy1:DialyData = DialyData(date: NSDate(), title: "おでかけ", text: "明美さんの本日の体調は良好!\nお日柄も良かったので、ともさんと一緒にお出かけ!\n最近はなかなか外に出たがらず、施設職員一同心配しておりましたので、安心した一日でした(^^)\n明日は、入浴日ですが、明日の具合はどうでしょうか?\n明美さん、今日も１日元気に過ごされました♫\n", imageString: "roujins")
var dialy2:DialyData = DialyData(date: NSDate(), title: "リハビリ", text: "今日も元気にリハビリを頑張りました.\nとても調子が良さそうです。", imageString: "kaigo")
var dialy3:DialyData = DialyData(date: NSDate(), title: "お花見", text: "今日はお花見に行きました", imageString: "roujins")
var dialy4:DialyData = DialyData(date: NSDate(), title: "リハビリ", text: "今日も元気にリハビリを頑張りました。とても調子が良さそうです。ほんとに。まじで。", imageString: "kaigo")
var dialys = [dialy1,dialy2,dialy3,dialy4]


var alldialy1:DialyData = DialyData(date: NSDate(), title: "みんなでお花見", text: "今日はみんなでお花見に行きました", imageString: "roujins")
var alldialy2:DialyData = DialyData(date: NSDate(), title: "リハビリ", text: "1週間に一度のお買い物Dayでした。\n明美さんは、体調が悪く半日、ベッドでお休みになられておられました。\nお昼ごはんはなんとか食べて、2時間ほど安静にしていましたところ、具合はだいぶ良くなり、午後のフリータイムにはベッドから起きてテレビを見られておりました!再来週は買い物行くぞー!\n", imageString: "kaigo")
var alldialy3:DialyData = DialyData(date: NSDate(), title: "お花見", text: "今日はお花見に行きました", imageString: "roujins")
var alldialy4:DialyData = DialyData(date: NSDate(), title: "リハビリ", text: "今日も元気にリハビリを頑張りました。とても調子が良さそうです.", imageString: "kaigo")
var alldialys = [alldialy1,alldialy2,alldialy3,alldialy4]







