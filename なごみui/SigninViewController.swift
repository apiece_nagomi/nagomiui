//
//  SigninViewController.swift
//  なごみui
//
//  Created by yu yamashiro on 2015/12/14.
//  Copyright © 2015年 yuyamashiro. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController {

    @IBOutlet weak var SigninLabel: UILabel!
    @IBOutlet weak var NameField: UITextField!
    @IBOutlet weak var PasswardField: UITextField!
    @IBOutlet weak var signinButton: UIButton!
    
    var appmode:Int?
    
    var BaseColor:UIColor = UIColor(red: 158/255, green: 99/255, blue: 191/255, alpha: 1.0/255)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NameField.placeholder = "入居者 名前"
        PasswardField.placeholder = "パスワード"
        self.view.backgroundColor = BaseColor
        SigninLabel.textColor = BaseColor
        
        //ボタンが押された時に呼ばれる関数を定義 関数名: コロンを忘れずに
        signinButton.addTarget(self, action:"tappedbutton:", forControlEvents:.TouchUpInside)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true , completion: nil)
    }

    internal func tappedbutton(sender:UIButton){
        
        if appmode != nil{
            switch appmode!{
            case 0:
                performSegueWithIdentifier("Syokuinsegue", sender: nil)
            case 1:
                performSegueWithIdentifier("Familysegue", sender: nil)
            default:
                print("error")
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
