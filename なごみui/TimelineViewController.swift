//
//  TimelineViewController.swift
//  なごみui
//
//  Created by yu yamashiro on 2015/12/15.
//  Copyright © 2015年 yuyamashiro. All rights reserved.
//

import UIKit

class TimelineViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var TableView: UITableView!
    var dialydatas:[DialyData] = dialys
    
       
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.setNavigationBarHidden(false, animated: true)
        
        dialydatas = dialys
        /*
        TableView.estimatedRowHeight = 320
        TableView.rowHeight = UITableViewAutomaticDimension*/
    }
    @IBAction func segmentedChanged(sender: AnyObject) {
        let control:UISegmentedControl = sender as! UISegmentedControl
        switch control.selectedSegmentIndex {
        case 0:
            self.dialydatas = dialys
        case 1:
            self.dialydatas = alldialys
        default:
            print("error")
        }
        self.TableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    //セクションの数
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    //cellの数
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dialydatas.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DialyTableViewCell",forIndexPath: indexPath) as! DialyTableViewCell
        //cell.data = dialydatas[indexPath.row]
        cell.setdata(dialydatas[indexPath.row])
        print("set")
        return cell
    }
    
    //高さの自動調節
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let data = self.dialydatas[indexPath.row]
        print("height")
        return DialyTableViewCell.heightForRow(tableView, data: data)
    }

    @IBAction func Backbutton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true , completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
