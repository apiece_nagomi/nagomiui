//
//  DialyTableViewCell.swift
//  なごみui
//
//  Created by yu yamashiro on 2015/12/17.
//  Copyright © 2015年 yuyamashiro. All rights reserved.
//

import UIKit

class DialyTableViewCell: UITableViewCell {
    @IBOutlet weak var dialytextview: UITextView!

    
    @IBOutlet weak var dialyImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dialyLabel: UILabel!
    
    var ddlabel:UILabel = UILabel(frame: CGRectMake(0,0,120,0))
    
    private var _datadialy:DialyData?
    var data: DialyData?{
        get{
            return _datadialy
        }
        set{
            _datadialy = data
            if let data = data{
                if let imagestring = data.Image{
                    self.dialyImage.image = UIImage(named: imagestring)
                }
                self.dateLabel.text = data.dateString
                self.titleLabel.text = data.Title
                self.titleLabel.sizeToFit()
                self.dialyLabel.text = data.Text
                self.layoutIfNeeded()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.dateLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.dateLabel.bounds)
        self.titleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.titleLabel.bounds)
        self.dialyLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.dialyLabel.bounds)
    }
    
    class func heightForRow(tableView: UITableView, data: DialyData?) -> CGFloat {
        struct Sizing {
            static var cell: DialyTableViewCell?
        }
        if Sizing.cell == nil {
            Sizing.cell = tableView.dequeueReusableCellWithIdentifier("DialyTableViewCell") as? DialyTableViewCell
        }
        if let cell = Sizing.cell {
            cell.frame.size.width = CGRectGetWidth(tableView.bounds)
            cell.setdata(data!)
            cell.data = data
            //systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)でautolayoutが適用されたあとのサイズを取得
            let size = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            return size.height
        }
        return 0
    }

    func setdata(data:DialyData){
        if let imgstr = data.Image{
            self.dialyImage.image = UIImage(named: imgstr)
            self.dialyImage.contentMode = UIViewContentMode.ScaleAspectFit
        }
        self.dateLabel.text = data.dateString
        self.titleLabel.text = data.Title
        self.dialyLabel.text = data.Text
        self.dialyLabel.sizeToFit()
        
        self.layoutIfNeeded()
    }
    
    
}
