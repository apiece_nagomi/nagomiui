//
//  FamilyHomeController.swift
//  なごみui
//
//  Created by yu yamashiro on 2015/12/28.
//  Copyright © 2015年 yuyamashiro. All rights reserved.
//

import UIKit

class FamilyHomeViewController: UIViewController {
    
    @IBOutlet weak var DialyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.setNavigationBarHidden(false, animated: true)
        
        //グラデーションの開始色
        let topColor = UIColor(red:0.00, green:0.0, blue:0.0, alpha:0)
        //グラデーションの開始色
        let bottomColor = UIColor(red:0.3, green:0.3, blue:0.3, alpha:1)
        
        //グラデーションの色を配列で管理
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        
        //グラデーションレイヤーを作成
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        
        //グラデーションの色をレイヤーに割り当てる
        gradientLayer.colors = gradientColors
        let navhight = self.navigationController!.navigationBar.frame.height
        let stahight = UIApplication.sharedApplication().statusBarFrame.height
        //グラデーションレイヤーをボタンサイズに
        gradientLayer.frame = CGRectMake(0, self.DialyButton.frame.height/2 + navhight + stahight,self.DialyButton.frame.width,self.DialyButton.frame.height/2)
        //ボタンの一番下のレイヤーに配置
        self.DialyButton.layer.insertSublayer(gradientLayer, atIndex: 0)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}