//
//  FacialSysViewController.swift
//  なごみui
//
//  Created by yu yamashiro on 2015/12/19.
//  Copyright © 2015年 yuyamashiro. All rights reserved.
//

import UIKit
import CoreImage

class FacialSysViewController: UIViewController,UIPopoverPresentationControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var FacialImage: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.FacialImage.userInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: "tappedimage:")
        self.FacialImage.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        CameraStart()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController!.setNavigationBarHidden(true, animated: true)
        print("view will disapper")
    }
    
    func tappedimage(sender:UITapGestureRecognizer){
    
        let popview = self.storyboard?.instantiateViewControllerWithIdentifier("facialpop")
        popview?.modalPresentationStyle = .Popover
        popview?.preferredContentSize = CGSizeMake(popview!.view.frame.size.width,popview!.view.frame.size.height/3)
        
        if let preController = popview?.popoverPresentationController{
            preController.permittedArrowDirections = .Up
            preController.sourceView = self.FacialImage
            preController.sourceRect = self.FacialImage.bounds
            preController.delegate = self
        }
        presentViewController(popview!, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "listFacial"{
            let vc = (segue.destinationViewController as! UITableViewController).popoverPresentationController
            if vc != nil{
                vc?.delegate = self
            }
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func CameraStart(){
        //カメラの利用
        let sorceType:UIImagePickerControllerSourceType = UIImagePickerControllerSourceType.Camera
        //カメラが利用可能かチェック
        if UIImagePickerController.isSourceTypeAvailable(sorceType){
            //インスタンスの生成
            let cameraPicker = UIImagePickerController()
            cameraPicker.sourceType = sorceType
            cameraPicker.delegate = self
            self.presentViewController(cameraPicker, animated: true, completion: nil)
        }else{
            print("error")
        }

    }
    //撮影完了した後に呼ばれる
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let cameraImage:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        print("Tap the [Save] to save a picture")
        /*let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.CameraImage = cameraImage*/
        self.FacialImage.image = cameraImage
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    // 撮影がキャンセルされた時に呼ばれる
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        print("Canceled")
    }
    
    
    @IBAction func BackCamera(sender: AnyObject) {
        CameraStart()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
