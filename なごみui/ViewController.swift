//
//  ViewController.swift
//  なごみui
//
//  Created by yu yamashiro on 2015/12/14.
//  Copyright © 2015年 yuyamashiro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var SyokuinButton: UIButton!
    @IBOutlet weak var FamilyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let signviewcontroller:SigninViewController = segue.destinationViewController as! SigninViewController
        if(segue.identifier == "SyokuinSegue"){
            signviewcontroller.BaseColor = UIColor(red: 158/255, green: 99/255, blue: 191/255, alpha: 1.0)
            signviewcontroller.appmode = 0
        }else{
            signviewcontroller.BaseColor = UIColor(red: 247/255, green: 154/255, blue: 174/255, alpha: 1.0)
            signviewcontroller.appmode = 1
        }
    }


}

